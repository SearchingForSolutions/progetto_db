<?php
// Initialize the session
session_start();

// If session variable is not set it will redirect to login page
if (!isset($_SESSION['username']) || empty($_SESSION['username'])) {
    header('location: index.html');
    exit;
}
?>

    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <title>Search</title>
        <link rel="stylesheet" href="assets/login/css/style.css">
        <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    </head>

    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <form action="" method="POST">

                            <div class="form-group">
                                <label for="sel1">Seleziona Categoria:</label>
                                <select class="form-control" id="sel1" name="sel_cat">

                                    <?php
                                       require 'php/shared/fetch_categories.php';
                                    ?>

                                </select>
                            </div>
                            <div class="form-group">
                                <label for="sel2">Seleziona Luogo:</label>
                                <select class="form-control" id="sel2" name="sel_place">

                                    <?php
                                       require 'php/shared/fetch_place.php';
                                    ?>

                                </select>
                            </div>


                        <button type="submit" class="btn btn-default">CERCA</button>
                    </form>
                </div>
                <div class="col-md-3"></div>
            </div>
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <form action="php/shared/vis_profilo_ph.php" action="POST">
                        <?php
                            require 'php/shared/create_rank_ph.php';
                        ?>
                    </form>
                </div>
                <div class="col-md-3"></div>
            </div>
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <a href="c_dashboard.php" class="btn btn-info" role="button">Torna</a>
                </div>
                <div class="col-md-3"></div>
            </div>
        </div>
    </body>
    <footer>
        <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
        <script src="assets/login/js/index.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    </footer>

    </html>
