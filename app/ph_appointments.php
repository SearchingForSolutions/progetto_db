<?php
// Initialize the session
session_start();

// If session variable is not set it will redirect to login page
if (!isset($_SESSION['username']) || empty($_SESSION['username'])) {
    header('location: index.html');
    exit;
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Invoice</title>
    <link rel="stylesheet" href="assets/login/css/style.css">
    <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-md-1"></div>
            <label for="comment">ATTENZIONE: verrà emessa fattura soltanto per quei servizi che sono offerti dal fotografo.<br><br></label>
            <div class="col-md-10">
                <label for="comment">Appuntamenti:</label>
               
                    <div class="col-xs-12">
                        <?php require 'php/ph/ph_print_appointments.php'; ?>
                    </div>

                </div>
            <div class="col-md-1"></div>
        </div>
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8"><a href="ph_dashboard.php" class="btn btn-info" role="button">Torna</a></div>
            <div class="col-md-2"></div>
        </div>
    </div>
</body>
<footer>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="assets/login/js/index.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
</footer>

</html>