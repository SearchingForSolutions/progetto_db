<?php
// Initialize the session
session_start();
 
// If session variable is not set it will redirect to login page
if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
  header("location: index.html");
  exit;
}
?>

    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <title>Welcome</title>
        <link rel="stylesheet" href="assets/login/css/style.css">
        <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    </head>

    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                </div>
                <div class="col-md-6">
                    <label for="comment">Servizi Offerti:</label>
                    <form action="php/ph/ph_rm_service.php" action="POST">
                        <?php require 'php/ph/ph_print_services.php'; ?>
                    </form>

                </div>
                <div class="col-md-3"></div>
            </div>
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <form action="php/ph/ph_ins_service.php" method="POST">
                        <div class="form-group">
                            <div class="form-group">
                                <label for="sel1">Seleziona Categoria:</label>
                                <select class="form-control" id="sel1" name="sel">

                                    <?php
                                        $sql = "SELECT cat_id, cat_type
                                                FROM categories";

                                        $result = $conn->query($sql);

                                        if($result->num_rows > 0){
                                            while($cat = $result->fetch_assoc()){
                                                echo "<option value=".$cat['cat_id'].">".$cat['cat_type']."</option>";
                                            }
                                        }else{
                                                echo "<p>non ci sono categorie (?) presenti<br></P>";
                                        }
                                        
                                        $result->close();
                                        
                                    ?>

                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="pwd">Prezzo Servizio:</label>
                            <input type="text" nameclass="form-control" name="price">
                        </div>
                        <div class="form-group">
                            <label for="pwd1">Descrizione Servizio:</label>
                            <textarea maxlength="100" class="form-control" rows="5" name="cat_desc"></textarea>
                        </div>
                        <button type="submit" class="btn btn-default">Aggiungi</button>
                    </form>
                </div>
                <div class="col-md-3"></div>

            </div>
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <a href="ph_dashboard.php" class="btn btn-info" role="button">Torna</a>
                </div>
                <div class="col-md-3"></div>
            </div>
        </div>
        <?php
            $conn->close();
        ?>
    </body>
    <footer>
        <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
        <script src="assets/login/js/index.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    </footer>

    </html>