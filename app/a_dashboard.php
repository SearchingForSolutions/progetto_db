<?php
// Initialize the session
session_start();

// If session variable is not set it will redirect to login page
if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
  header("location: index.html");
  exit;
}

?>

    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <title>Admin Dashboard</title>
        <link rel="stylesheet" href="assets/login/css/style.css">
        <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <style type="text/css">
            #btn {
                font: 14px sans-serif;
                text-align: center;
            }
        </style>
    </head>

    <body>
        <div id="btn" class="page-header">
            <h1>Ciao,
                <b>
                    <?php echo $_SESSION['username'].", il tuo id è ".$_SESSION['user_id']?>
                </b>. Benvenuto nella tua Dashboard.</h1>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <a href="php/shared/logout.php" id="btn" class="btn btn-danger">Esci</a>
                    <a href="php/admin/admin_show_comment.php" class="btn btn-info" role="button">Visualizza Commenti</a>
                    <a href="php/admin/admin_show_picture.php" class="btn btn-info" role="button">Visualizza Foto</a>
                    <a href="php/admin/admin_show_statics.php" class="btn btn-info" role="button">Visualizza Statistiche</a>
                </div>
                <div class="col-md-2"></div>
            </div>
        </div>
    </body>
    <footer>
        <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
        <script src="assets/login/js/index.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    </footer>

    </html>
