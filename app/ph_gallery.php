<?php
// Initialize the session
session_start();

// If session variable is not set it will redirect to login page
if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
  header("location: index.html");
  exit;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Ph Gallery</title>
  <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/style.css" rel="stylesheet">
</head>

<body>
<div class="container">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <form action="php/ph/ph_uploads.php" method="post" enctype="multipart/form-data">
                <h2>Upload Foto</h2>
                <label for="fileSelect">Scegli file:</label><br>
                <input class="form-control-file" type="file" name="photo" id="fileSelect"><br>
                <div class="form-group">
                                <label for="sel1">Seleziona Categoria:</label>
                                <select class="form-control" id="sel1" name="sel_cat">

                                    <?php
                                       require 'php/shared/fetch_categories.php';
                                    ?>

                                </select>
                            </div>
                            <div class="form-group">
                            <label for="comment">Descrizione:</label>
                            <textarea name="desc" maxlength="48" class="form-control" rows="2" ></textarea>
                        </div>
                <button type="submit" name="submit" class="btn btn-primary">Upload</button>
                <p><strong>Note:</strong> Sono ammessi solo file .jpg, .jpeg, .gif, .png dimensione max 5 MB.</p>
            </form>
        </div>
        <div class="col-md-3"></div>
    </div>
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6"><h2>Foto Caricate:</h2></div>
        <div class="col-md-3"></div>
    </div>
    <?php require 'php/shared/fetch_up_images.php';?>
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6"><br><a href="ph_dashboard.php" class="btn btn-info" role="button">Torna Home</a></div>
        <div class="col-md-3"></div>
    </div>
</div>
</body>

<footer>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="assets/bootstrap/js/bootstrap.min.js"></script>
</footer>

</html>


