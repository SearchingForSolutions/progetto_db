-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Giu 25, 2018 alle 11:37
-- Versione del server: 10.1.32-MariaDB
-- Versione PHP: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `photo_advisor`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `admin`
--

CREATE TABLE `admin` (
  `user_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dump dei dati per la tabella `admin`
--

INSERT INTO `admin` (`user_id`) VALUES
(1);

-- --------------------------------------------------------

--
-- Struttura della tabella `appointments`
--

CREATE TABLE `appointments` (
  `date` date NOT NULL,
  `hours` time NOT NULL,
  `client_id` int(10) NOT NULL,
  `ph_id` int(10) NOT NULL,
  `service_id` int(10) NOT NULL,
  `approved` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dump dei dati per la tabella `appointments`
--

INSERT INTO `appointments` (`date`, `hours`, `client_id`, `ph_id`, `service_id`, `approved`) VALUES
('2018-06-12', '20:00:00', 7, 5, 7, 0),
('2018-06-15', '15:00:00', 4, 3, 3, 1),
('2018-10-20', '17:00:00', 2, 3, 1, 2);

-- --------------------------------------------------------

--
-- Struttura della tabella `categories`
--

CREATE TABLE `categories` (
  `cat_id` int(2) NOT NULL,
  `cat_type` varchar(20) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dump dei dati per la tabella `categories`
--

INSERT INTO `categories` (`cat_id`, `cat_type`) VALUES
(1, 'matrimonio'),
(2, 'fidanzamento'),
(3, 'neonati'),
(4, 'gravidanza'),
(5, 'bambini'),
(6, 'famiglie'),
(7, 'ritratti di persone'),
(8, 'moda'),
(9, 'animali'),
(10, 'cerimonie religiose'),
(11, 'compleanni'),
(12, 'anniversari'),
(13, 'pubblicita\''),
(14, 'corporate'),
(15, 'still life'),
(16, 'eventi aziendali');

-- --------------------------------------------------------

--
-- Struttura della tabella `clients`
--

CREATE TABLE `clients` (
  `client_id` int(10) NOT NULL,
  `client_type` varchar(20) COLLATE utf8_bin NOT NULL,
  `fiscal_code_vat` varchar(20) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dump dei dati per la tabella `clients`
--

INSERT INTO `clients` (`client_id`, `client_type`, `fiscal_code_vat`) VALUES
(2, '', ''),
(4, 'Privato', '1010110'),
(7, '', '');

-- --------------------------------------------------------

--
-- Struttura della tabella `images`
--

CREATE TABLE `images` (
  `image_id` int(10) NOT NULL,
  `image_desc` varchar(50) COLLATE utf8_bin NOT NULL,
  `image_type` varchar(4) COLLATE utf8_bin NOT NULL,
  `images_path` varchar(200) COLLATE utf8_bin NOT NULL,
  `ph_id` int(10) NOT NULL,
  `up_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dump dei dati per la tabella `images`
--

INSERT INTO `images` (`image_id`, `image_desc`, `image_type`, `images_path`, `ph_id`, `up_id`) VALUES
(1, '3', 'imag', 'upload/3/battesimo1.jpg', 3, 1),
(2, '3', 'imag', 'upload/3/battesimo2.jpg', 3, 2),
(3, '3', 'imag', 'upload/3/battesimo3.jpg', 3, 3),
(4, '3', 'imag', 'upload/3/battesimo4.jpg', 3, 4),
(5, '3', 'imag', 'upload/3/battesimo5.jpg', 3, 5),
(6, '2', 'imag', 'upload/3/fidanzamento1.jpg', 3, 6),
(7, '2', 'imag', 'upload/3/fidanzamento2.jpg', 3, 7),
(8, '2', 'imag', 'upload/3/fidanzamento3.jpg', 3, 8),
(9, '1', 'imag', 'upload/3/matrimonio1.jpg', 3, 9),
(10, '1', 'imag', 'upload/3/matrimonio2.jpg', 3, 10),
(11, '9', 'imag', 'upload/5/animali1.jpg', 5, 11),
(12, '9', 'imag', 'upload/5/animali2.jpg', 5, 12),
(13, '9', 'imag', 'upload/5/animali5.jpg', 5, 13),
(14, '13', 'imag', 'upload/5/pubblicitÃ 2.jpg', 5, 14),
(15, '13', 'imag', 'upload/5/pubblicitÃ 5.jpg', 5, 15),
(16, '7', 'imag', 'upload/5/ritratto5.jpg', 5, 16),
(17, '7', 'imag', 'upload/5/ritratto2.jpg', 5, 17),
(18, '7', 'imag', 'upload/5/ritratto1.jpg', 5, 18);

-- --------------------------------------------------------

--
-- Struttura della tabella `invoices_ph`
--

CREATE TABLE `invoices_ph` (
  `n_invoice` int(10) NOT NULL,
  `ammount` int(5) NOT NULL,
  `date` date NOT NULL,
  `ph_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dump dei dati per la tabella `invoices_ph`
--

INSERT INTO `invoices_ph` (`n_invoice`, `ammount`, `date`, `ph_id`) VALUES
(1, 100, '2019-05-22', 3);

-- --------------------------------------------------------

--
-- Struttura della tabella `media`
--

CREATE TABLE `media` (
  `media_id` int(10) NOT NULL,
  `media_type` varchar(10) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dump dei dati per la tabella `media`
--

INSERT INTO `media` (`media_id`, `media_type`) VALUES
(1, 'Fotografie');

-- --------------------------------------------------------

--
-- Struttura della tabella `offers`
--

CREATE TABLE `offers` (
  `service_id` int(10) NOT NULL,
  `ph_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dump dei dati per la tabella `offers`
--

INSERT INTO `offers` (`service_id`, `ph_id`) VALUES
(1, 3),
(2, 3),
(3, 3),
(4, 5),
(5, 5),
(6, 5);

-- --------------------------------------------------------

--
-- Struttura della tabella `photographers`
--

CREATE TABLE `photographers` (
  `ph_id` int(10) NOT NULL,
  `ph_desc` varchar(400) COLLATE utf8_bin NOT NULL,
  `ph_pfe` int(3) NOT NULL,
  `ph_status` int(1) NOT NULL,
  `place_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dump dei dati per la tabella `photographers`
--

INSERT INTO `photographers` (`ph_id`, `ph_desc`, `ph_pfe`, `ph_status`, `place_id`) VALUES
(3, 'fotografo professionista specializzato in foto di matrimoni, fidanzamenti e battesimi.', 0, 1, 8),
(5, 'specializzato in foto di animali, pubblicitÃ  e ritratti di persone.', 0, 1, 1),
(6, '', 0, 0, NULL);

-- --------------------------------------------------------

--
-- Struttura della tabella `place`
--

CREATE TABLE `place` (
  `place_id` int(10) NOT NULL,
  `place_name` varchar(20) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dump dei dati per la tabella `place`
--

INSERT INTO `place` (`place_id`, `place_name`) VALUES
(1, 'Piemonte'),
(2, 'Valle D\'Aosta'),
(3, 'Lombardia'),
(4, 'Trentino-Alto Adige'),
(5, 'Veneto'),
(6, 'Friuli-Venezia Giuli'),
(7, 'Liguria'),
(8, 'Emilia-Romagna'),
(9, 'Toscana'),
(10, 'Umbria'),
(11, 'Marche'),
(12, 'Lazio'),
(13, 'Abruzzo'),
(14, 'Molise'),
(15, 'Campania'),
(16, 'Puglia'),
(17, 'Basilicata'),
(18, 'Calabria'),
(19, 'Sicilia'),
(20, 'Sardegna');

-- --------------------------------------------------------

--
-- Struttura della tabella `ratings`
--

CREATE TABLE `ratings` (
  `ratings_id` int(10) NOT NULL,
  `client_id` int(10) NOT NULL,
  `ph_id` int(10) NOT NULL,
  `comment` varchar(200) COLLATE utf8_bin NOT NULL,
  `vote` int(1) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dump dei dati per la tabella `ratings`
--

INSERT INTO `ratings` (`ratings_id`, `client_id`, `ph_id`, `comment`, `vote`, `date`) VALUES
(1, 2, 3, 'molto bravo e professionale.', 1, '2018-06-25'),
(2, 4, 3, 'ottime capacitÃ .', 4, '2018-06-25');

-- --------------------------------------------------------

--
-- Struttura della tabella `services`
--

CREATE TABLE `services` (
  `service_id` int(10) NOT NULL,
  `price_range` int(5) NOT NULL,
  `service_desc` varchar(100) COLLATE utf8_bin NOT NULL,
  `cat_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dump dei dati per la tabella `services`
--

INSERT INTO `services` (`service_id`, `price_range`, `service_desc`, `cat_id`) VALUES
(1, 1500, 'book fotografico per matrimoni.', 1),
(2, 1000, 'book della festa di fidanzamento, album compreso.', 2),
(3, 500, 'foto di battesimi e per album ricordo.', 3),
(4, 200, 'ottimi ritratti in primo piano, per curriculum e book.', 7),
(5, 150, 'foto di animali per documentari.', 9),
(6, 800, 'foto per pubblicitÃ .', 13);

-- --------------------------------------------------------

--
-- Struttura della tabella `service_charge`
--

CREATE TABLE `service_charge` (
  `client_id` int(10) NOT NULL,
  `date` date NOT NULL,
  `service_id` int(10) NOT NULL,
  `payments_id` int(10) NOT NULL,
  `invoice_type` int(11) NOT NULL,
  `payed` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dump dei dati per la tabella `service_charge`
--

INSERT INTO `service_charge` (`client_id`, `date`, `service_id`, `payments_id`, `invoice_type`, `payed`) VALUES
(2, '2018-10-20', 1, 0, 0, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `uploads`
--

CREATE TABLE `uploads` (
  `up_id` int(10) NOT NULL,
  `ph_id` int(10) NOT NULL,
  `date_and_hours` datetime NOT NULL,
  `cat_id` int(10) NOT NULL,
  `media_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dump dei dati per la tabella `uploads`
--

INSERT INTO `uploads` (`up_id`, `ph_id`, `date_and_hours`, `cat_id`, `media_id`) VALUES
(1, 3, '2018-06-25 00:00:00', 3, 1),
(2, 3, '2018-06-25 00:00:00', 3, 1),
(3, 3, '2018-06-25 00:00:00', 3, 1),
(4, 3, '2018-06-25 00:00:00', 3, 1),
(5, 3, '2018-06-25 00:00:00', 3, 1),
(6, 3, '2018-06-25 00:00:00', 2, 1),
(7, 3, '2018-06-25 00:00:00', 2, 1),
(8, 3, '2018-06-25 00:00:00', 2, 1),
(9, 3, '2018-06-25 00:00:00', 1, 1),
(10, 3, '2018-06-25 00:00:00', 1, 1),
(11, 5, '2018-06-25 00:00:00', 9, 1),
(12, 5, '2018-06-25 00:00:00', 9, 1),
(13, 5, '2018-06-25 00:00:00', 9, 1),
(14, 5, '2018-06-25 00:00:00', 13, 1),
(15, 5, '2018-06-25 00:00:00', 13, 1),
(16, 5, '2018-06-25 00:00:00', 7, 1),
(17, 5, '2018-06-25 00:00:00', 7, 1),
(18, 5, '2018-06-25 00:00:00', 7, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `users`
--

CREATE TABLE `users` (
  `user_id` int(10) NOT NULL,
  `name` varchar(20) COLLATE utf8_bin NOT NULL,
  `last_name` varchar(20) COLLATE utf8_bin NOT NULL,
  `username` varchar(20) COLLATE utf8_bin NOT NULL,
  `password` varchar(255) COLLATE utf8_bin NOT NULL,
  `mobile` varchar(13) COLLATE utf8_bin NOT NULL,
  `email` varchar(25) COLLATE utf8_bin NOT NULL,
  `address` varchar(30) COLLATE utf8_bin NOT NULL,
  `permission` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dump dei dati per la tabella `users`
--

INSERT INTO `users` (`user_id`, `name`, `last_name`, `username`, `password`, `mobile`, `email`, `address`, `permission`) VALUES
(1, 'admin', 'admin', 'admin', '$2y$10$1hl7up6cmFpAfAPQkDD40.szWIZXoi2PslWL11g2peW4x44mcSoMu', '7418527896', 'admin@admin.admin', 'admin', 2),
(2, 'client', 'client', 'client', '$2y$10$sgLyvVZ01JD14Ify5j.UZu.7C8lwLVpj3wLALLa9ydcgpjD26qKGC', '789963542', 'cliet@yahoo.it', 'via del pero 102', 0),
(3, 'ph', 'ph', 'ph', '$2y$10$zj1ZRikHkTncV9ejASBTie9NSO8TsCAPStf9Q3Q.Jl3EmOHsjAJPW', '5465987854', 'ph@gmai.com', 'via del melo 101', 1),
(4, 'lorenzo', 'casini', 'caso', '$2y$10$c2.f4nDKioYeNlUO51Tr/OBoMv7IacMKP5AAsl5vPPyveiriSaWhu', '4567899631', 'casini@hotmail.it', 'via alemagni 12', 0),
(5, 'umberto', 'baldini', 'baldo', '$2y$10$zQIuUcrLb4A6rev8DovRAOFTaS3Dv0ib7BYcxi2bFSjOeWAh.PAqC', '4569871235', 'umberto@yahoo.it', 'via marconi 12', 1),
(6, 'alberto', 'alberto', 'nonattivo', '$2y$10$aSiJ4zf04Hi851iUDV2kaeb3wCb9VdzxSz8aOsCLdZ.yIibmuDXE.', '7894561235', 'alberto@hotmail.com', 'via garibaldi 11', 1),
(7, 'federica', 'focaccia', 'foca', '$2y$10$sr14A.xq5ZFFgocWUcdQW.TUh2VQvBRxSOedrMPMZesWyXpS2Eyvy', '9637418527', 'focaccina@hotmail.it', 'via palermitana 50', 0);

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`user_id`);

--
-- Indici per le tabelle `appointments`
--
ALTER TABLE `appointments`
  ADD PRIMARY KEY (`date`,`hours`,`ph_id`),
  ADD KEY `FK_app_cli` (`client_id`),
  ADD KEY `FK_app_ph` (`ph_id`);

--
-- Indici per le tabelle `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indici per le tabelle `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`client_id`);

--
-- Indici per le tabelle `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`image_id`),
  ADD KEY `FK_img_photographers` (`ph_id`),
  ADD KEY `FK_img_up` (`up_id`);

--
-- Indici per le tabelle `invoices_ph`
--
ALTER TABLE `invoices_ph`
  ADD PRIMARY KEY (`n_invoice`),
  ADD KEY `FK_inv_ph` (`ph_id`);

--
-- Indici per le tabelle `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`media_id`);

--
-- Indici per le tabelle `offers`
--
ALTER TABLE `offers`
  ADD PRIMARY KEY (`service_id`,`ph_id`),
  ADD KEY `FK_off_ph` (`ph_id`);

--
-- Indici per le tabelle `photographers`
--
ALTER TABLE `photographers`
  ADD PRIMARY KEY (`ph_id`),
  ADD KEY `FK_ph_place` (`place_id`);

--
-- Indici per le tabelle `place`
--
ALTER TABLE `place`
  ADD PRIMARY KEY (`place_id`);

--
-- Indici per le tabelle `ratings`
--
ALTER TABLE `ratings`
  ADD PRIMARY KEY (`ratings_id`),
  ADD KEY `FK_ra_clients` (`client_id`),
  ADD KEY `FK_ra_ph` (`ph_id`);

--
-- Indici per le tabelle `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`service_id`),
  ADD KEY `FK_serv_cat` (`cat_id`);

--
-- Indici per le tabelle `service_charge`
--
ALTER TABLE `service_charge`
  ADD PRIMARY KEY (`client_id`,`date`,`service_id`),
  ADD KEY `Fk_servcha_serv` (`service_id`);

--
-- Indici per le tabelle `uploads`
--
ALTER TABLE `uploads`
  ADD PRIMARY KEY (`up_id`),
  ADD KEY `FK_up_cat` (`cat_id`),
  ADD KEY `Fk_up_ph` (`ph_id`),
  ADD KEY `FK_up_media` (`media_id`);

--
-- Indici per le tabelle `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `admin`
--
ALTER TABLE `admin`
  MODIFY `user_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT per la tabella `categories`
--
ALTER TABLE `categories`
  MODIFY `cat_id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT per la tabella `images`
--
ALTER TABLE `images`
  MODIFY `image_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT per la tabella `invoices_ph`
--
ALTER TABLE `invoices_ph`
  MODIFY `n_invoice` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT per la tabella `media`
--
ALTER TABLE `media`
  MODIFY `media_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT per la tabella `place`
--
ALTER TABLE `place`
  MODIFY `place_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT per la tabella `ratings`
--
ALTER TABLE `ratings`
  MODIFY `ratings_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT per la tabella `services`
--
ALTER TABLE `services`
  MODIFY `service_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT per la tabella `uploads`
--
ALTER TABLE `uploads`
  MODIFY `up_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT per la tabella `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `admin`
--
ALTER TABLE `admin`
  ADD CONSTRAINT `FK_admin_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`);

--
-- Limiti per la tabella `appointments`
--
ALTER TABLE `appointments`
  ADD CONSTRAINT `FK_app_client` FOREIGN KEY (`client_id`) REFERENCES `clients` (`client_id`),
  ADD CONSTRAINT `FK_app_ph` FOREIGN KEY (`ph_id`) REFERENCES `photographers` (`ph_id`);

--
-- Limiti per la tabella `clients`
--
ALTER TABLE `clients`
  ADD CONSTRAINT `FK_client_user` FOREIGN KEY (`client_id`) REFERENCES `users` (`user_id`);

--
-- Limiti per la tabella `images`
--
ALTER TABLE `images`
  ADD CONSTRAINT `FK_img_ph` FOREIGN KEY (`ph_id`) REFERENCES `photographers` (`ph_id`),
  ADD CONSTRAINT `FK_img_up` FOREIGN KEY (`up_id`) REFERENCES `uploads` (`up_id`);

--
-- Limiti per la tabella `invoices_ph`
--
ALTER TABLE `invoices_ph`
  ADD CONSTRAINT `FK_inv_ph` FOREIGN KEY (`ph_id`) REFERENCES `photographers` (`ph_id`);

--
-- Limiti per la tabella `offers`
--
ALTER TABLE `offers`
  ADD CONSTRAINT `FK_off_ph` FOREIGN KEY (`ph_id`) REFERENCES `photographers` (`ph_id`);

--
-- Limiti per la tabella `photographers`
--
ALTER TABLE `photographers`
  ADD CONSTRAINT `FK_ph_place` FOREIGN KEY (`place_id`) REFERENCES `place` (`place_id`),
  ADD CONSTRAINT `FK_ph_user` FOREIGN KEY (`ph_id`) REFERENCES `users` (`user_id`);

--
-- Limiti per la tabella `ratings`
--
ALTER TABLE `ratings`
  ADD CONSTRAINT `FK_ra_client` FOREIGN KEY (`client_id`) REFERENCES `clients` (`client_id`),
  ADD CONSTRAINT `FK_ra_ph` FOREIGN KEY (`ph_id`) REFERENCES `photographers` (`ph_id`);

--
-- Limiti per la tabella `services`
--
ALTER TABLE `services`
  ADD CONSTRAINT `Fk_serv_cat` FOREIGN KEY (`service_id`) REFERENCES `categories` (`cat_id`);

--
-- Limiti per la tabella `service_charge`
--
ALTER TABLE `service_charge`
  ADD CONSTRAINT `FK_servcha_serv` FOREIGN KEY (`service_id`) REFERENCES `services` (`service_id`);

--
-- Limiti per la tabella `uploads`
--
ALTER TABLE `uploads`
  ADD CONSTRAINT `FK_up_cat` FOREIGN KEY (`cat_id`) REFERENCES `categories` (`cat_id`),
  ADD CONSTRAINT `FK_up_media` FOREIGN KEY (`media_id`) REFERENCES `media` (`media_id`),
  ADD CONSTRAINT `FK_up_ph` FOREIGN KEY (`ph_id`) REFERENCES `photographers` (`ph_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
