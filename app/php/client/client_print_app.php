<?php
// Initialize the session
session_start();

// If session variable is not set it will redirect to login page
if (!isset($_SESSION['username']) || empty($_SESSION['username'])) {
    header('location: index.html');
    exit;
}
?>

    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <title>Appuntamenti</title>
        <link rel="stylesheet" href="../../assets/login/css/style.css">
        <link href="../../assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    </head>

    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                <h2>Appuntamenti Accettati:</h2>
                    <form action="#" action="POST">
                        <?php
                        require '../shared/db_config.php';
    
                        $sql="SELECT A.date,A.hours,U.name,U.last_name
                        FROM appointments A
                        INNER JOIN users U ON A.client_id = U.user_id
                        WHERE A.approved > 0 AND A.client_id = ".$_SESSION['user_id'];
    
                        $result = $conn->query($sql);
    
                        if($result->num_rows >0){
    
                            echo "<table class='table'>";
                            echo"<thead>";
                                echo "<tr>";
                                    echo "<th scope='col'>Nome</th>";
                                    echo "<th scope='col'>Cognome</th>";
                                    echo "<th scope='col'>Data</th>";
                                    echo "<th scope='col'>Ora</th>";
                                echo "</tr>";
                            echo"</thead>";
                            while($row = $result->fetch_array()){
                            echo"<tbody>";
                                echo "<tr>";
                                    echo "<td>" . $row['name'] . "</td>";
                                    echo "<td>" . $row['last_name'] . "</td>";
                                    echo "<td>" . $row['date'] . "</td>";
                                    echo "<td>" . $row['hours'] . "</td>";
                                    echo "<td><button type='submit' value='bubba' name='click' class='btn btn-primary'>Cancella</button></td>";
                                echo "</tr>";
                            echo"</tbody>";
                            }
                            echo "</table>";
                            
                            
                            $result->close();
                        }else{
                            echo "errore query";
                        }
                        $conn->close();
                        ?>
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                <h2>Appuntamenti In Attesa:</h2>
                    <form action="#" action="POST">
                        <?php
                        require '../shared/db_config.php';
    
                        $sql="SELECT A.date,A.hours,U.name,U.last_name
                        FROM appointments A
                        INNER JOIN users U ON A.client_id = U.user_id
                        WHERE A.approved < 1 AND A.client_id = ".$_SESSION['user_id'];
    
                        $result = $conn->query($sql);
    
                        if($result->num_rows >0){
    
                            echo "<table class='table'>";
                            echo"<thead>";
                                echo "<tr>";
                                    echo "<th scope='col'>Nome</th>";
                                    echo "<th scope='col'>Cognome</th>";
                                    echo "<th scope='col'>Data</th>";
                                    echo "<th scope='col'>Ora</th>";
                                echo "</tr>";
                            echo"</thead>";
                            while($row = $result->fetch_array()){
                            echo"<tbody>";
                                echo "<tr>";
                                    echo "<td>" . $row['name'] . "</td>";
                                    echo "<td>" . $row['last_name'] . "</td>";
                                    echo "<td>" . $row['date'] . "</td>";
                                    echo "<td>" . $row['hours'] . "</td>";
                                    echo "<td><button type='submit' value='bubba' name='click' class='btn btn-primary'>Cancella</button></td>";
                                echo "</tr>";
                            echo"</tbody>";
                            }
                            echo "</table>";
                            
                            
                            $result->close();
                        }else{
                            echo '<p><b>Nessun Appuntamento in attesa.</b></p>';
                        }
                        $conn->close();
                        ?>
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <a href="../../c_dashboard.php" class="btn btn-info" role="button">Torna</a>
                </div>
                <div class="col-md-3"></div>
            </div>
        </div>
    </body>
    <footer>
        <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
        <script src="../../assets/login/js/index.js"></script>
        <script src="../../assets/bootstrap/js/bootstrap.min.js"></script>
    </footer>

    </html>
