<?php
session_start();

// If session variable is not set it will redirect to login page
if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
  header("location: index.html");
  exit;
}

require_once '../shared/db_config.php';

$sql = "SELECT DISTINCT SC.date, Cat.cat_type, S.service_desc, U.name, U.last_name, SC.payments_id, SC.invoice_type, SC.payed, S.service_id
        FROM service_charge SC INNER JOIN clients C ON SC.client_id = C.client_id 
        INNER JOIN offers O ON O.service_id = SC.service_id
        INNER JOIN services S ON S.service_id = SC.service_id
        INNER JOIN categories Cat ON Cat.cat_id = S.cat_id
        INNER JOIN photographers P ON P.ph_id = O.ph_id
        INNER JOIN users U ON U.user_id = O.ph_id
        WHERE C.client_id = ".$_SESSION['user_id'];

    if($result = $conn->query($sql)){
        if($result->num_rows > 0){
            echo "<table class='table'>";
            echo"<thead>";
                echo "<tr>";
                    echo "<th scope='col'>Data</th>";
                    echo "<th scope='col'>categoria</th>";
                    echo "<th scope='col'>descrizione del servizio</th>";
                    echo "<th scope='col'>Nome ph</th>";
                    echo "<th scope='col'>Cognome ph</th>";
                    echo "<th scope='col'>ID fattura/scontrino</th>";
                    echo "<th scope='col'>tipo di fattura</th>";
                echo "</tr>";
            echo"</thead>";
            while($row = $result->fetch_array()){
            echo "<form action='cl_pay_invoice.php' action='POST'>";
            echo"<tbody>";
                echo "<tr>";
                    echo "<td>" . $row['date'] . "</td>";
                    echo "<td>" . $row['cat_type'] . "</td>";
                    echo "<td>" . $row['service_desc'] . "</td>";
                    echo "<td>" . $row['name'] . "</td>";
                    echo "<td>" . $row['last_name'] ."</td>";
                    echo "<td>" . $row['payments_id'] ."</td>";
                    echo "<td>" . $row['invoice_type'] ."</td>";
                    echo "<input type='date' name='date' value='".$row['date']."' hidden>";
                    echo "<input type='int' name='client_id' value='".$_SESSION['user_id']."' hidden>";
                    echo "<input type='int' name='service_id' value='".$row['service_id']."' hidden>";
                    if($row['payed'] == 0) {
                            echo "<td><button type='submit' value='' name='click' class='btn btn-primary'>paga</button></td>";
                    }else{
                        echo "<th scope='col'>pagato</th>";
                    }
                echo "</tr>";
            echo"</tbody>";
            echo "</form>";
            }
            echo "</table>";
            // Free result set
            $result->close();
        } else{
            echo "Non ci sono fatture attualmente disponibili";
        }
    } else{
        echo "ERROR: Could not able to execute $sql. " . $conn->error;
    }
    
    // Close connection
    $conn->close();


?>