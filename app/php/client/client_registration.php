
<?php
/* Attempt MySQL server connection.*/
require_once '../shared/db_config.php';

// Prepare an insert statement
$sql = "INSERT INTO users (name,last_name,username,password,mobile,email,address,permission) VALUES (?,?,?,?,?,?,?,?)";
 
if($stmt = $conn->prepare($sql)){
    // Bind variables to the prepared statement as parameters
    $stmt->bind_param("sssssssi",$name,$lname,$username,$password,$mobile,$email,$address,$client);
    
    // Set parameters
    $client=0;
    $name = $_REQUEST["name"];
    $lname = $_REQUEST["lastname"];
    $username = $_REQUEST["username"];
    $mobile = $_REQUEST["mobile"];
    $address = $_REQUEST["address"];
    $email = $_REQUEST["email"];
    $password = password_hash($_REQUEST["password"], PASSWORD_DEFAULT);
    
    // Attempt to execute the prepared statement
    if($stmt->execute()){
        echo "Records inserted successfully in users.<br>";
    } else{
        echo "ERROR: Could not execute query: $sql. " . $conn->error;
    }
} else{
    echo "ERROR: Could not prepare query: $sql. " . $conn->error;
}
 
// Close statement
$stmt->close();

// Prepare an insert statement
$sql = "INSERT INTO clients (client_id) VALUES (?)";
 
if($stmt = $conn->prepare($sql)){
    // Bind variables to the prepared statement as parameters
    $stmt->bind_param("i",$last_insert_id);
    
    //Catch the last insert id
    $last_insert_id = $conn->insert_id;
    
    // Attempt to execute the prepared statement
    if($stmt->execute()){
        echo "Records inserted successfully in clients.";
    } else{
        echo "ERROR: Could not execute query: $sql. " . $conn->error;
    }
} else{
    echo "ERROR: Could not prepare query: $sql. " . $conn->error;
}
 
// Close statement 
$stmt->close();
 
 
// Close connection
$conn->close();
?>