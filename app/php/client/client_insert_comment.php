<?php
// Initialize the session
session_start();

// If session variable is not set it will redirect to login page
if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
  header("location: ../../index.html");
  exit;
}

require_once '../shared/db_config.php';

$sql = "INSERT INTO ratings (client_id,ph_id,comment,vote,date) VALUES (?,?,?,?,?)";
 
if($stmt = $conn->prepare($sql)){
    // Bind variables to the prepared statement as parameters
    $stmt->bind_param("iisis", $client_id, $ph_id, $comment,$vote,$date);
    
    $client_id = $_SESSION['user_id'];
    $ph_id = $_SESSION['ph_id'];
    $comment = $_REQUEST['comment'];
    $vote = $_REQUEST['sel1'];
    $date = date("Y-m-d");
    $stmt->execute();
    
    echo "Commento Inserito con successo.";
} else{
    echo "ERROR: Could not prepare query: $sql. " . $conn->error;
}
 
// Close statement
$stmt->close();
 
// Close connection
$conn->close();

header("location: ../shared/vis_profilo_ph.php?click=".$ph_id."");
?>


