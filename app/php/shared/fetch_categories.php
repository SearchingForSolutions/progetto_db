<?php

require 'db_config.php';
$sql = "SELECT cat_id, cat_type
        FROM categories";

$result = $conn->query($sql);

if($result->num_rows > 0){
    while($cat = $result->fetch_assoc()){
        echo "<option value=".$cat['cat_id'].">".$cat['cat_type']."</option>";
    }
    $result->close();
}else{
        echo "<p>non ci sono categorie (?) presenti<br></P>";
}
$conn->close();

?>