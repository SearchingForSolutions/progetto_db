<?php

// Include config file
require 'db_config.php';

$username = $_REQUEST['username'];
$password = $_REQUEST['password'];

// Prepare a select statement
$sql = 'SELECT user_id,username,password,permission FROM users WHERE username = ?';

if ($stmt = $conn->prepare($sql)) {
    // Bind variables to the prepared statement as parameters
    $stmt->bind_param('s', $param_username);

    // Set parameters
    $param_username = $username;

    // Attempt to execute the prepared statement
    if ($stmt->execute()) {
        // Store result
        $stmt->store_result();

        // Check if username exists, if yes then verify password
        if ($stmt->num_rows == 1) {
            // Bind result variables
            $stmt->bind_result($user_id, $username, $hashed_password, $perm);
            if ($stmt->fetch()) {
                if (password_verify($password, $hashed_password)) {
                    /* Password is correct, so start a new session and
                    save the username to the session */
                    session_start();
                    $_SESSION['username'] = $username;
                    $_SESSION['user_id'] = $user_id;
                    switch ($perm) {
                        case 0:
                            header('location: ../../c_dashboard.php');
                            break;
                        case 1:
                            header('location: ../../ph_dashboard.php');
                            break;
                        case 2:
                            header('location: ../../a_dashboard.php');
                            break;
                        
                        default:
                            header('location: ../../index.html');
                            break;
                    }
                } else {
                    // Display an error message if password is not valid
                    echo 'The password you entered was not valid.';
                }
            }
        } else {
            // Display an error message if username doesn't exist
            echo 'No account found with that username.';
        }
    } else {
        echo 'Oops! Something went wrong. Please try again later.';
    }
}

// Close statement
$stmt->close();

// Close connection
$conn->close();
?>