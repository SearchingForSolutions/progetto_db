<?php
// Initialize the session
session_start();

$ph_id = $_REQUEST['click']; 
$_SESSION['ph_id']=$ph_id;

// If session variable is not set it will redirect to login page
if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
  header("location: ../../index.html");
  exit;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Photo Advisor</title>
  <link href="../../assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="../../assets/style.css" rel="stylesheet">
</head>

<body>
<div class="container">
    <?php require 'fetch_ph_info.php';?>
    <?php require 'fetch_ph_images.php';?>
    <?php require 'fetch_ph_ratings.php';?> 
    <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                <label for="pwd">Lascia un Commento</label><br>
                    <form action="../client/client_insert_comment.php" method="GET">
                        <div class="form-group">
                            <div class="form-group">
                              <label for="sel1">Voto:</label>
                              <select class="form-control" id="sel1" name="sel1">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                              </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="pwd1">Commento:</label>
                            <textarea maxlength="200" class="form-control" rows="5" name="comment"></textarea>
                        </div>
                        <button type="submit" class="btn btn-default">Aggiungi</button>
                    </form>
                </div>
                <div class="col-md-3"></div>
            </div>   
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                <form action="../client/cl_insert_app.php" method="POST">
                <div class ="form-section">
                <label for="datehour">Richiedi un appuntamento:</label>
                </div>
                <div class="form-section" id="datehour">
                    <span class="fa fa-user-circle input-icon"></span>
                    <input type="date" name="date" placeholder="date">
                    <span class="fa fa-user-circle input-icon"></span>
                    <input type="time" name="hour" placeholder="hour">
                </div>
                <div class="form-group">
                    <label for="selcat">Seleziona Categoria:</label>
                    <select class="form-control" id="selcat" name="sel_cat">

                        <?php require 'fetch_categories.php';?>

                    </select>
                </div>
                <input type="int" value=<?php echo $_SESSION['user_id'];?> name="client_id" hidden>
                <input type="int" value=<?php echo $_SESSION['ph_id'];?> name="ph_id" hidden>
                <button type="submit" class="btn btn-default">Prenota</button>
            </form>     
                </div>
                <div class="col-md-3"></div>
            </div>   

            

    <a href="../../c_search.php" class="btn btn-info" role="button">Torna</a>    
</div>
  
</body>

<footer>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="../../assets/bootstrap/js/bootstrap.min.js"></script>
</footer>

</html>


