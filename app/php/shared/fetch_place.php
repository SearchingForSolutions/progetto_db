<?php

require 'db_config.php';

$sql = "SELECT place_id, place_name
        FROM place";

$result = $conn->query($sql);

if($result->num_rows > 0){
    while($cat = $result->fetch_assoc()){
        echo "<option value=".$cat['place_id'].">".$cat['place_name']."</option>";
    }
    $result->close();
}else{
        echo "<p>non ci sono luoghi (?) presenti<br></P>";
}

$conn->close();

?>