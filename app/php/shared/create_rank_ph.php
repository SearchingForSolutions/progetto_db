<?php
require 'db_config.php';

$cat=$_REQUEST['sel_cat'];
$place=$_REQUEST['sel_place'];

if(isset($_REQUEST['sel_cat'])&&isset($_REQUEST['sel_place'])){
        // Attempt select query execution
            $sql = "SELECT P.ph_id,P.ph_pfe,U.name,U.last_name
            FROM photographers P
            INNER JOIN users U ON P.ph_id =  U.user_id
            INNER JOIN place Pl ON P.place_id = Pl.place_id
            INNER JOIN offers Of ON P.ph_id = Of.ph_id
            INNER JOIN services S ON Of.service_id = S.service_id
            INNER JOIN categories C ON S.cat_id = C.cat_id
            WHERE P.ph_status = 1
            AND C.cat_id = ".$cat."
            AND Pl.place_id = ".$place."
            ORDER BY P.ph_pfe DESC";

    if($result = $conn->query($sql)){
        if($result->num_rows > 0){
            echo "<table class='table'>";
            echo"<thead>";
                echo "<tr>";
                    echo "<th scope='col'></th>";
                    echo "<th scope='col'>Nome</th>";
                    echo "<th scope='col'>Cognome</th>";
                    echo "<th scope='col'>Punteggio</th>";
                    echo "<th scope='col'>Link</th>";
                echo "</tr>";
            echo"</thead>";
            while($row = $result->fetch_array()){
            echo"<tbody>";
                echo "<tr>";
                    echo "<td>" . $row['ph_id'] . "</td>";
                    echo "<td>" . $row['name'] . "</td>";
                    echo "<td>" . $row['last_name'] . "</td>";
                    echo "<td>" . $row['ph_pfe'] . "</td>";
                    echo "<td><button type='submit' value='".$row['ph_id']."' name='click' class='btn btn-primary'>Mostra Profilo</button></td>";
                echo "</tr>";
            echo"</tbody>";
            }
            echo "</table>";
            // Free result set
            $result->free();
        } else{
            echo "Non ci sono fotografi per i filtri selezionati";
        }
    } else{
        echo "ERROR: Could not able to execute $sql. " . $conn->error;
    }

    // Close connection
   
}
$conn->close();
?>