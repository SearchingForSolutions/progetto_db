<?php
// Initialize the session
session_start();

// If session variable is not set it will redirect to login page
if (!isset($_SESSION['username']) || empty($_SESSION['username'])) {
    header('location: index.html');
    exit;
}
?>

    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <title>Show Picture</title>
        <link rel="stylesheet" href="../../assets/login/css/style.css">
        <link href="../../assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    </head>

    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <form action="admin_del_picture.php" action="POST">
                        <?php
                        require '../shared/db_config.php';
    
                        $sql="SELECT I.image_id,I.image_desc,I.images_path 
                              FROM images I";
    
                        $result = $conn->query($sql);
    
                        if($result->num_rows >0){
    
                            echo "<table class='table'>";
                            echo"<thead>";
                                echo "<tr>";
                                    echo "<th scope='col'>ID</th>";
                                    echo "<th scope='col'>Descrizione</th>";
                                    echo "<th scope='col'>Immagine</th>";
                                    echo "<th scope='col'>Modera</th>";
                                echo "</tr>";
                            echo"</thead>";
                            while($row = $result->fetch_array()){
                            echo"<tbody>";
                                echo "<tr>";
                                    echo "<td>" . $row['image_id'] . "</td>";
                                    echo "<td>" . $row['image_desc'] . "</td>";
                                    echo '<td><img src="../../'.$row['images_path'].'" alt="'.$row['image_desc'].'" border=3 height=50 width=100></img></td>';
                                    echo "<td><button type='submit' value='".$row['image_id']."' name='click' class='btn btn-primary'>Cancella</button></td>";
                                echo "</tr>";
                            echo"</tbody>";
                            }
                            echo "</table>";
                            
                            
                            $result->close();
                        }else{
                            echo "errore query";
                        }
                        $conn->close();
                        ?>
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <a href="../../a_dashboard.php" class="btn btn-info" role="button">Torna</a>
                </div>
                <div class="col-md-3"></div>
            </div>
        </div>
    </body>
    <footer>
        <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
        <script src="../../assets/login/js/index.js"></script>
        <script src="../../assets/bootstrap/js/bootstrap.min.js"></script>
    </footer>

    </html>
