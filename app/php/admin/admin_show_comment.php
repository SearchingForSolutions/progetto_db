<?php
// Initialize the session
session_start();

// If session variable is not set it will redirect to login page
if (!isset($_SESSION['username']) || empty($_SESSION['username'])) {
    header('location: index.html');
    exit;
}
?>

    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <title>Search</title>
        <link rel="stylesheet" href="../../assets/login/css/style.css">
        <link href="../../assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    </head>

    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <form action="admin_del_comment.php" action="POST">
                        <?php
                        require '../shared/db_config.php';
    
                        $sql="SELECT R.ratings_id,R.comment,R.vote,R.date,U.name,U.last_name
                        FROM ratings R
                        INNER JOIN users U ON R.client_id = U.user_id";
    
                        $result = $conn->query($sql);
    
                        if($result->num_rows >0){
    
                            echo "<table class='table'>";
                            echo"<thead>";
                                echo "<tr>";
                                    echo "<th scope='col'>Nome</th>";
                                    echo "<th scope='col'>Cognome</th>";
                                    echo "<th scope='col'>Data</th>";
                                    echo "<th scope='col'>Voto</th>";
                                    echo "<th scope='col'>Commento</th>";
                                    echo "<th scope='col'>Modera</th>";
                                echo "</tr>";
                            echo"</thead>";
                            while($row = $result->fetch_array()){
                            echo"<tbody>";
                                echo "<tr>";
                                    echo "<td>" . $row['name'] . "</td>";
                                    echo "<td>" . $row['last_name'] . "</td>";
                                    echo "<td>" . $row['date'] . "</td>";
                                    echo "<td>" . $row['vote'] . "</td>";
                                    echo "<td>" . $row['comment'] . "</td>";
                                    echo "<td><button type='submit' value='".$row['ratings_id']."' name='click' class='btn btn-primary'>Cancella</button></td>";
                                echo "</tr>";
                            echo"</tbody>";
                            }
                            echo "</table>";
                            
                            
                            $result->close();
                        }else{
                            echo "errore query";
                        }
                        $conn->close();
                        ?>
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <a href="../../a_dashboard.php" class="btn btn-info" role="button">Torna</a>
                </div>
                <div class="col-md-3"></div>
            </div>
        </div>
    </body>
    <footer>
        <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
        <script src="../../assets/login/js/index.js"></script>
        <script src="../../assets/bootstrap/js/bootstrap.min.js"></script>
    </footer>

    </html>
