<?php

require '../shared/db_config.php';

$sql = "SELECT COUNT(S.client_id) AS num
        FROM service_charge S
        WHERE S.payed = 1";

$result = $conn->query($sql);

if($result->num_rows > 0){
    while($row = $result->fetch_assoc()){
        echo '<h3>Numero Servizi Pagati: '.$row['num'].'</h3>';
    }
    $result->close();
}else{
        echo "<p>non ci sono servizi pagati registrati</p>";
}
$conn->close();

?>