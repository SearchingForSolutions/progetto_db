<?php
    require_once 'php/shared/db_config.php';
    
    // Attempt select query execution
    $sql = "SELECT A.date, A.hours, A.client_id, U.name, U.last_name, A.approved, A.service_id
            FROM appointments A INNER JOIN photographers P ON A.ph_id = P.ph_id 
            INNER JOIN users U ON U.user_id = A.client_id
            WHERE P.ph_id = ".$_SESSION['user_id'];

    if($result = $conn->query($sql)){
        if($result->num_rows > 0){
            echo "<table class='table'>";
            echo"<thead>";
                echo "<tr>";
                    echo "<th scope='col' width='235px'>Data</th>";
                    echo "<th scope='col'>Ora</th>";
                    echo "<th scope='col' width='300px'>ID cliente</th>";
                    echo "<th scope='col' width='300px'>Nome cliente</th>";
                    echo "<th scope='col' width='300px'>Cognome cliente</th>";
                    echo "<th scope='col' width='300px'>ID servizio</th>";
                echo "</tr>";
            echo"</thead>";
            while($row = $result->fetch_array()){
            echo "<form action='php/ph/ph_app_answer.php' action='POST'>";
            echo"<tbody>";
                echo "<tr>";
                    echo "<td>" . $row['date'] . "</td>";
                    echo "<td>" . $row['hours'] . "</td>";
                    echo "<td>" . $row['client_id'] . "</td>";
                    echo "<td>" . $row['name'] . "</td>";
                    echo "<td>" . $row['last_name'] ."</td>";
                    echo "<td>" . $row['service_id'] ."</td>";
                    echo "<input type='date' name='date' value='".$row['date']."' hidden>";
                    echo "<input type='time' name='hours' value='".$row['hours']."' hidden>";
                    echo "<input type='int' name='ph_id' value='".$_SESSION['user_id']."' hidden>";
                    echo "<input type='int' name='service_id' value='".$row['service_id']."' hidden>";
                    echo "<input type='int' name='client_id' value='".$row['client_id']."' hidden>";
                    switch ($row['approved']) {
                        case 0:
                            echo "<td><button type='submit' value='1' name='click' class='btn btn-primary'>accetta</button></td>";
                            echo "<td><button type='submit' value='0' name='click' class='btn btn-primary'>rifiuta</button></td>";
                            break;
                        case 1:
                            echo "<td><button type='submit' value='2' name='click' class='btn btn-primary'>emetti fattura</button></td>";
                            break;
                        case 2:
                            echo "<td><button type='submit' value='3' name='click' class='btn btn-primary'>visualizza fattura</button></td>";
                            break;
                    }
                echo "</tr>";
            echo"</tbody>";
            echo "</form>";
            }
            echo "</table>";
            // Free result set
            $result->close();
        } else{
            echo "Non ci sono appuntamenti attualmente disponibili";
        }
    } else{
        echo "ERROR: Could not able to execute $sql. " . $conn->error;
    }
    
    // Close connection
    $conn->close();
?>