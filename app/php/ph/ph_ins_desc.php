<?php
// Initialize the session
session_start();

// If session variable is not set it will redirect to login page
if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
  header("location: ../../index.html");
  exit;
}

require_once '../shared/db_config.php';

$sql = "UPDATE photographers
        SET ph_desc = '".$_REQUEST['desc']."'
        WHERE ph_id = ".$_SESSION['user_id'];

$conn->query($sql);

$conn->close();

header("location: ../../ph_dashboard.php");

?>