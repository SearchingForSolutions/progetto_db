<?php

//echo $_REQUEST['date']." ".$_REQUEST['hours']." ".$_REQUEST['ph_id']." ".$_REQUEST['click'];

require '../shared/db_config.php';


switch ($_REQUEST['click']) {
    case 0://cancella appuntamento

        $sql = "DELETE A.*
        FROM appointments A
        WHERE date = '".$_REQUEST['date']."'
        AND hours = '".$_REQUEST['hours']."'
        AND ph_id = ".$_REQUEST['ph_id'];

        if ($conn->query($sql) === TRUE) {
            header('location: ../../ph_appointments.php');
        } else {
            echo "Error registering your operation: " . $conn->error."<br>";
            echo "div class='col-md-8'><a href='../../ph_appointments.php' class='btn btn-info' role='button'>Torna</a></div>";
        }

        break;
        // fine caso 1

    case 1://accetta appuntamento

        $sql = "UPDATE appointments
                SET approved = 1
                WHERE date = '".$_REQUEST['date']."'
                AND hours = '".$_REQUEST['hours']."'
                AND ph_id = ".$_REQUEST['ph_id'];

        if ($conn->query($sql) === TRUE) {
            header('location: ../../ph_appointments.php');
        } else {
            echo "Error registering your operation: " . $conn->error."<br>";
            echo "div class='col-md-8'><a href='../../ph_appointments.php' class='btn btn-info' role='button'>Torna</a></div>";
        }

        break;
        // fine caso 1

    case 2://emetti fattura
        
        $sql ="INSERT INTO service_charge(client_id, date, service_id, payments_id, invoice_type, payed)
                VALUE (?,?,?,?,?,?)";

        if($stmt = $conn->prepare($sql)){
            $stmt->bind_param("isiiii", $client_id, $date, $service_id, $payment_id, $invoice_type, $payed);

            $client_id = $_REQUEST['client_id'];
            $date = $_REQUEST['date'];
            $service_id = $_REQUEST['service_id'];
            $payment_id = 0;
            $invoice_type = 0;
            $payed = 0;

            if($stmt->execute()){
                echo "Servizio erogato registrato.<br>";

                $sql = "UPDATE appointments
                SET approved = 2
                WHERE date = '".$_REQUEST['date']."'
                AND hours = '".$_REQUEST['hours']."'
                AND ph_id = ".$_REQUEST['ph_id'];

                if ($conn->query($sql) === TRUE) {
                    echo "appuntamento aggiornato erogato registrato.<br>";
                } else {
                    echo "Error registering your operation: " . $conn->error."<br>";
                    echo "div class='col-md-8'><a href='../../ph_appointments.php' class='btn btn-info' role='button'>Torna</a></div>";
                }
            } else{
                echo "ERROR: Could not execute query: $sql. " . $conn->error;
            }
            $stmt->close();
            header('location: ../../ph_appointments.php');
        }

        break;
        // fine caso 2

    case 3://visualizza fattura

        $sql ="SELECT client_id, date, service_id, payments_id, invoice_type, payed
               FROM service_charge
               WHERE date = '".$_REQUEST['date']."'
               AND service_id = ".$_REQUEST['service_id']."
               AND client_id = ".$_REQUEST['client_id'];

        if($result = $conn->query($sql)){
            $row = $result->fetch_assoc();
                echo "<table class='table'>";
                echo"<thead>";
                    echo "<tr>";
                        echo "<th scope='col'>ID cliente</th>";
                        echo "<th scope='col'>data</th>";
                        echo "<th scope='col'>ID servizio</th>";
                        echo "<th scope='col'>ID fattura</th>";
                        echo "<th scope='col'>tipo fattura</th>";
                        echo "<th scope='col'>pagato</th>";
                    echo "</tr>";
                echo"</thead>";
                echo"<tbody>";
                    echo "<tr>";
                        echo "<td>" . $row['client_id'] . "</td>";
                        echo "<td>" . $row['date'] . "</td>";
                        echo "<td>" . $row['service_id'] . "</td>";
                        echo "<td>" . $row['payments_id'] . "</td>";
                        echo "<td>" . $row['invoice_type'] ."</td>";
                        echo "<td>" . $row['payed'] ."</td>";                            
                    echo "</tr>";
                echo"</tbody>";
                echo "</table>";
                // Free result set
                $result->close();                
            }else {
                echo "Error searching your invoice: " . $conn->error."<br>";
            } 
            echo "<div class='col-md-8'><a href='../../ph_appointments.php' class='btn btn-info' role='button'>Torna</a></div>";
        break;
        // fine caso 3

    }//fine switch
$conn->close();
?>