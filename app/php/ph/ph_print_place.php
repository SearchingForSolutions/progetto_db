<?php

// If session variable is not set it will redirect to login page
if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
  header("location: ../../index.html");
  exit;
}

require_once 'php/shared/db_config.php';

$sql = "SELECT P.place_name
        FROM place P INNER JOIN photographers Ph ON P.place_id = Ph.place_id
        WHERE Ph.ph_id = ".$_SESSION['user_id'];

$result = $conn->query($sql);

if($desc = $result->fetch_assoc()){
    echo $desc['place_name'];
}else{
    echo "luogo assente";
}

$result->close();

?>