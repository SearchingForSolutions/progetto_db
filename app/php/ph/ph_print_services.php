<?php
 
    require_once 'php/shared/db_config.php';

    $sql = "SELECT service_id, price_range, service_desc, cat_id
            FROM services
            WHERE service_id = ANY (SELECT service_id
                                FROM offers
                                WHERE ph_id = ".$_SESSION['user_id'].")";

    $result = $conn->query($sql);

    if($result->num_rows > 0){
            echo "<table class='table'>";
            echo"<thead>";
                echo "<tr>";
                    echo "<th scope='col'>id servizio</th>";
                    echo "<th scope='col'>fascia prezzo</th>";
                    echo "<th scope='col'>descrizione</th>";
                    echo "<th scope='col'>id categoria</th>";
                echo "</tr>";
            echo"</thead>";

            while($service = $result->fetch_assoc()){
                echo"<tbody>";
                    echo "<tr>";
                        echo "<td>" . $service['service_id'] . "</td>";
                        echo "<td>" . $service['price_range'] . "</td>";
                        echo "<td>" . $service['service_desc'] . "</td>";
                        echo "<td>" . $service['cat_id'] . "</td>";
                        echo "<td><button type='submit' value='".$service['cat_id']."' name='click' class='btn btn-primary'>Cancella</button></td>";
                    echo "</tr>";
                echo"</tbody>";
            }

            echo "</table>";
    }else{
            echo "<p>non ci sono servizi presenti<br></P>";
    }   
    // Close statement
    $result->close();           
?>