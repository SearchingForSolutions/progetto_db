<?php
// Initialize the session
session_start();

// If session variable is not set it will redirect to login page
if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
  header("location: index.html");
  exit;
}

 $ph_id = $_SESSION['user_id'];
 //$cat = $_REQUEST['sel_cat'];

 // Check if the form was submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){
    // Check if file was uploaded without errors
    if(isset($_FILES["photo"]) && $_FILES["photo"]["error"] == 0){
        $allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "gif" => "image/gif", "png" => "image/png");
        $filename = $_FILES["photo"]["name"];
        $filetype = $_FILES["photo"]["type"];
        $filesize = $_FILES["photo"]["size"];
    
        // Verify file extension
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        if(!array_key_exists($ext, $allowed)) die("Errore: inserisci un formato corretto.");
    
        // Verify file size - 5MB maximum
        $maxsize = 5 * 1024 * 1024;
        if($filesize > $maxsize) die("Errore: File piu' grande di 5mb.");
    
        // Verify MYME type of the file
        if(in_array($filetype, $allowed)){
            // Check whether file exists before uploading it
            if(file_exists("../../upload/".$ph_id ."/". $_FILES["photo"]["name"])){
                echo $_FILES["photo"]["name"] . " gia' presente.";
            } else{
                if (!mkdir("../../upload/".$ph_id , 777, true)) {
                   // echo('Impossibile creare cartella.');
                }
                if(move_uploaded_file($_FILES["photo"]["tmp_name"], "../../upload/".$ph_id."/".$_FILES["photo"]["name"])){
                    echo "La tua foto è stata caricata con successo.";
                }
                else{
                    echo "errore spostamento cartella";
                }
                
            } 
        } else{
            echo "Errore: C'e' stato un errore durante l'upload della foto. Ritenta."; 
        }
    } else{
        echo "Errore: " . $_FILES["photo"]["error"];
    }

}
/*Save data to DB*/

require_once '../shared/db_config.php';

$sql = "INSERT INTO uploads (ph_id,date_and_hours,cat_id,media_id) VALUES (?,?,?,?)";
 
if($stmt = $conn->prepare($sql)){
    // Bind variables to the prepared statement as parameters
    $stmt->bind_param("isii",$ph_id,$date,$cat,$media_id);
    
    $ph_id = $_SESSION['user_id'];
    $date = date("Y-m-d");
    $cat = $_REQUEST['sel_cat'];
    $media_id=1;
    
    $stmt->execute();
    
    echo "upload registrato";
} else{
    echo "ERROR: Could not prepare query: $sql. " . $conn->error;
}
 
// Close statement
$stmt->close();

$sql = "INSERT INTO images (image_desc,image_type,images_path,ph_id,up_id) VALUES (?,?,?,?,?)";
 
if($stmt = $conn->prepare($sql)){
    // Bind variables to the prepared statement as parameters
    $stmt->bind_param("sssii",$image_desc,$image_type,$image_path,$ph_id,$up_id);
    
    $image_desc=$_REQUEST['sel_cat'];
    $image_type=$_FILES["photo"]["type"];
    $image_path="upload/".$ph_id."/".$_FILES["photo"]["name"];
    $up_id = $conn->insert_id;
    $stmt->execute();
    
    echo "foto registrata";
} else{
    echo "ERROR: Could not prepare query: $sql. " . $conn->error;
}
 
// Close statement
$stmt->close();
 
// Close connection
$conn->close();

//header("location: ../../ph_gallery.php");
?>