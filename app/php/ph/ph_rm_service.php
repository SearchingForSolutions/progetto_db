<?php 

// Initialize the session
session_start();

// If session variable is not set it will redirect to login page
if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
    header("location: ../../index.html");
    exit;
}

require_once '../shared/db_config.php';

$sql = "DELETE O.*,S.*
        FROM offers O INNER JOIN services S ON O.service_id = S.service_id
        WHERE O.ph_id = ".$_SESSION['user_id']."
        AND S.cat_id = ".$_REQUEST['click'];

if($conn->query($sql) === TRUE) {
    echo "servizio cancellato <br>";
}else{
    echo "ERROR deleting record: ". $conn->error;
}

$conn->close();

echo "<div class='col-md-6'><a href='../../ph_service.php' class='btn btn-info' role='button'>Torna</a></div>";

?>