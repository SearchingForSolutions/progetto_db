<?php
    require_once 'php/shared/db_config.php';
    
    // Attempt select query execution
    $sql = "SELECT invoices_ph.n_invoice,invoices_ph.ammount,invoices_ph.date,users.name,users.last_name 
            FROM invoices_ph INNER JOIN users ON invoices_ph.ph_id =  users.user_id 
            WHERE invoices_ph.ph_id=".$_SESSION['user_id'];

    if($result = $conn->query($sql)){
        if($result->num_rows > 0){
            echo "<table class='table'>";
            echo"<thead>";
                echo "<tr>";
                    echo "<th scope='col'>N° Fattura</th>";
                    echo "<th scope='col'>Nome</th>";
                    echo "<th scope='col'>Cognome</th>";
                    echo "<th scope='col'>Data</th>";
                    echo "<th scope='col'>Totale</th>";
                echo "</tr>";
            echo"</thead>";
            while($row = $result->fetch_array()){
            echo"<tbody>";
                echo "<tr>";
                    echo "<td>" . $row['n_invoice'] . "</td>";
                    echo "<td>" . $row['name'] . "</td>";
                    echo "<td>" . $row['last_name'] . "</td>";
                    echo "<td>" . $row['date'] . "</td>";
                    echo "<td>" . $row['ammount'] ."€</td>";
                echo "</tr>";
            echo"</tbody>";
            }
            echo "</table>";
            // Free result set
            $result->free();
        } else{
            echo "Non ci sono fatture attualmente disponibili";
        }
    } else{
        echo "ERROR: Could not able to execute $sql. " . $conn->error;
    }
    
    // Close connection
    $conn->close();
?>