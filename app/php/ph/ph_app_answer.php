

<!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <title>Search</title>
        <link rel="stylesheet" href="../../assets/login/css/style.css">
        <link href="../../assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    </head>

    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">

                <?php require 'ph_gest_risp.php'?>
                
                </div>
                <div class="col-md-2"></div>
            </div>
        </div>
    </body>
    <footer>
        <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
        <script src="../../assets/login/js/index.js"></script>
        <script src="../../assets/bootstrap/js/bootstrap.min.js"></script>
    </footer>

    </html>
