<?php
    // Initialize the session
    session_start();
    
    // If session variable is not set it will redirect to login page
    if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
    header("location: ../../index.php");
    exit;
    }

    require_once '../shared/db_config.php';

    $sqlcontrol = "SELECT S.service_id, O.ph_id
                   FROM services S INNER JOIN offers O ON S.service_id = O.service_id
                   WHERE S.cat_id = ".$_REQUEST['sel']."
                   AND O.ph_id = ".$_SESSION['user_id'];
                   
               
    if($control = $conn->query($sqlcontrol)){
        if($control->num_rows == 0){
            $sqlserv = "INSERT INTO services (price_range, service_desc, cat_id)
                VALUES(?,?,?)";

            $sqloff = "INSERT INTO offers (service_id, ph_id)
                        VALUES(?,?)";
            
            if($stmt = $conn->prepare($sqlserv)){
                // Bind variables to the prepared statement as parameters
                $stmt->bind_param("isi", $price_range, $service_desc, $cat_id);

                $price_range = $_REQUEST['price'];
                $service_desc = $_REQUEST['cat_desc'];
                $cat_id = $_REQUEST['sel'];

                if($stmt->execute()){
                    echo "Servizio registrato.<br>";
                } else{
                    echo "ERROR: Could not execute query: $sqlserv. " . $conn->error;
                }

                $stmt->close();
            }
            

            if($stmt2 = $conn->prepare($sqloff)){

                $stmt2->bind_param("ii",$serv_id, $ph_id);
                $serv_id = $conn->insert_id;
                $ph_id = $_SESSION['user_id'];

                if($stmt2->execute()){
                    echo "Offerta registrata.<br>";
                } else{
                    echo "ERROR: Could not execute query: $sqloff. " . $conn->error;
                }

                $stmt2->close();
            }

        }else{
            echo "servizio già presente";
        }
    }else{
        echo "controllo non possibile";
    }


    
    $conn->close();

    echo "<div class='col-md-6'><a href='../../ph_service.php' class='btn btn-info' role='button'>Torna</a></div>";
?>  