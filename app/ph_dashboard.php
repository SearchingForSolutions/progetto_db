<?php
// Initialize the session
session_start();

// If session variable is not set it will redirect to login page
if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
  header("location: index.html");
  exit;
}
?>

    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <title>Photographers Dashboard</title>
        <link rel="stylesheet" href="assets/login/css/style.css">
        <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <style type="text/css">
            #btn {
                font: 14px sans-serif;
                text-align: center;
            }
        </style>
    </head>

    <body>
        <div id="btn" class="page-header">
            <h1>Ciao,
                <b>
                    <?php echo $_SESSION['username'].", il tuo id è ".$_SESSION['user_id']?>
                </b>. Benvenuto nella tua Dashboard.
            </h1>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <a href="php/shared/logout.php" id="btn" class="btn btn-danger">Esci</a>
                    <a href="ph_appointments.php" class="btn btn-info" role="button">Appuntamenti</a>
                    <a href="ph_gallery.php" class="btn btn-info" role="button">Galleria</a>
                    <a href="ph_invoice.php" class="btn btn-info" role="button">Fatture</a>
                    <a href="ph_service.php" class="btn btn-info" role="button">Servizi</a>
                </div>
                <div class="col-md-3"></div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-3">
                </div>
                <div class="col-md-6">
                    <label for="comment">Descrizione Attuale: 
                        <?php require "php/ph/ph_print_desc.php"?>
                    </label>
                </div>
                <div class="col-md-3"></div>
            </div>
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <form action="php/ph/ph_ins_desc.php" method="POST">
                        <div class="form-group">
                            <label for="comment">Aggiorna Descrizione:</label>
                            <textarea name="desc" maxlength="400" class="form-control" rows="5" id="comment"></textarea>
                        </div>
                        <div class="form-section btn-container">
                            <input type="submit" value="Aggiorna">
                        </div>
                    </form>
                </div>
                <div class="col-md-3"></div>
            </div>
            <div class="row">
                <div class="col-md-3">
                </div>
                <div class="col-md-6">
                    <label for="comment">Luogo Attuale: 
                        <?php require "php/ph/ph_print_place.php"?>
                    </label>
                </div>
                <div class="col-md-3"></div>
            </div>
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    
                    <form action="php/ph/ph_ins_place.php" method="POST">
                        <div class="form-group">
                            <label 
                            <label for="sel2">Aggiorna Luogo:</label>
                            <select class="form-control" id="sel2" name="place_id">
                                    <?php require "php/shared/fetch_place.php"?>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-default">AGGIORNA</button>
                    </form>                        

                </div>
                <div class="col-md-3"></div>
            </div>
        </div>
    </body>
    <footer>
        <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
        <script src="assets/login/js/index.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    </footer>

    </html>
