<?php
// Initialize the session
session_start();

// If session variable is not set it will redirect to login page
if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
  header("location: index.html");
  exit;
}

?>

    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <title>Client Dashboard</title>
        <link rel="stylesheet" href="assets/login/css/style.css">
        <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <style type="text/css">
            #btn {
                font: 14px sans-serif;
                text-align: center;
            }
        </style>
    </head>

    <body>
        <div id="btn" class="page-header">
            <h1>Ciao,
                <b>
                    <?php echo $_SESSION['username'].", il tuo id è ".$_SESSION['user_id']?>
                </b>. Benvenuto nella tua Dashboard.</h1>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <a href="php/shared/logout.php" id="btn" class="btn btn-danger">Esci</a>
                    <a href="php/client/client_print_app.php" class="btn btn-info" role="button">Appuntamenti</a>
                    <a href="php/client/cl_vis_payments.php" class="btn btn-info" role="button">Fatture</a>
                    <a href="c_search.php" class="btn btn-info" role="button">Cerca Fotografo</a>
                </div>
                <div class="col-md-3"></div>
            </div>
            <div class="row">
                <div class="col-md-3">
                </div>
                <div class="col-md-6">
                    <label for="comment">P.iva o CF Attuale: 
                        <?php require 'php/client/client_print_fiscal.php';?>
                    </label>
                </div>
                <div class="col-md-3"></div>
            </div>
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <form action="php/client/client_ins_fiscal.php" method="POST">
                        <div class="form-group">
                            <label for="comment">Aggiorna P.iva o CF:</label>
                            <input type="text" name="piva" id="cf">
                        </div>
                        <div class="form-section btn-container">
                            <input type="submit" value="Aggiorna">
                        </div>
                    </form>
                </div>
                <div class="col-md-3"></div>
            </div>
            <div class="row">
                <div class="col-md-3">
                </div>
                <div class="col-md-6">
                    <label for="comment">Tipo di Cliente: 
                        <?php require 'php/client/client_print_type.php';?>
                    </label>
                </div>
                <div class="col-md-3"></div>
            </div>
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    
                    <form action="php/client/client_ins_cf.php" method="POST">
                        <div class="form-group">
                            <label 
                            <label for="sel2">Aggiorna Tipo Cliente:</label>
                            <select class="form-control" id="sel2" name="client_type">
                                 <option value="Privato">Privato</option>
                                 <option value="Azienda">Azienda</option>   
                            </select>
                        </div>
                        <div class="form-section btn-container">
                            <input type="submit" value="Aggiorna">
                        </div>
                    </form>                        

                </div>
                <div class="col-md-3"></div>
            </div>
        </div>
    </body>
    <footer>
        <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
        <script src="assets/login/js/index.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    </footer>

    </html>
