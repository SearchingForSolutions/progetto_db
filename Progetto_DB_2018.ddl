-- *********************************************
-- * SQL MySQL generation                      
-- *--------------------------------------------
-- * DB-MAIN version: 10.0.3              
-- * Generator date: Aug 23 2017              
-- * Generation date: Wed Jun 20 12:09:54 2018 
-- * LUN file: /home/lorenzo/Desktop/progetto_db/Progetto_DB_2018.lun 
-- * Schema: Logico/Manual 
-- ********************************************* 


-- Database Section
-- ________________ 

create database Logico;
use Logico;


-- Tables Section
-- _____________ 

create table ADMIN (
     user_id int not null,
     constraint FKUSE_ADM_ID primary key (user_id));

create table APPOINTEMENT (
     appointement_id int not null,
     date date not null,
     hours varchar(10) not null,
     user_id int not null,
     ACC_user_id int not null,
     constraint IDAPPOINTEMENT primary key (date, hours, ACC_user_id));

create table CATEGORIES (
     cat_types varchar(20) not null,
     cat_id int not null,
     constraint IDCATEGORIES primary key (cat_id));

create table CLIENTS (
     user_id int not null,
     client_type varchar(15) not null,
     fiscal code/VAT number varchar(11) not null,
     constraint FKUSE_CLI_ID primary key (user_id));

create table IMAGES (
     images_id int not null,
     images_desc varchar(50) not null,
     images_type varchar(10) not null,
     images_path varchar(100) not null,
     media_id int not null,
     user_id int not null,
     constraint IDIMAGES primary key (images_id));

create table INVOICES_PH (
     amount int not null,
     n_invoice int not null,
     date date not null,
     user_id int not null,
     constraint IDINVOICES primary key (n_invoice));

create table MEDIA (
     media_type varchar(10) not null,
     media_id int not null,
     upload_id char(1) not null,
     constraint IDMEDIA_ID primary key (media_id));

create table OFFERS (
     service_id int not null,
     user_id int not null,
     constraint IDOFFERS primary key (service_id, user_id));

create table PHOTOGRAPHERS (
     user_id int not null,
     ph_desc varchar(400) not null,
     VAT_number varchar(11) not null,
     ph_pfe int not null,
     ph_status char not null,
     place_id int not null,
     constraint FKUSE_PHO_ID primary key (user_id));

create table PLACE (
     place_id int not null,
     place_name varchar(20) not null,
     constraint IDPLACE primary key (place_id));

create table RATINGS (
     rating_id int not null,
     user_id int,
     DES_user_id int not null,
     comment varchar(200) not null,
     date char(1) not null,
     approved char not null,
     vote int not null,
     constraint IDRATINGS primary key (rating_id));

create table SERVICE CHARGE (
     date date not null,
     user_id int not null,
     payments_id char(1) not null,
     invoice_type char(1) not null,
     service_id int not null,
     constraint IDSERVICE CHARGE primary key (date, user_id, service_id));

create table SERVICES (
     service_id int not null,
     price_range int not null,
     service_desc varchar(200) not null,
     cat_id int not null,
     constraint IDSERVICE_ID primary key (service_id));

create table UPLOADS (
     user_id int not null,
     upload_id char(1) not null,
     date_and_hour date not null,
     cat_id int not null,
     constraint IDUPLOADS_ID primary key (upload_id));

create table USER (
     user_id int not null,
     name char(1) not null,
     lastname char(1) not null,
     username varchar(20) not null,
     password varchar(20) not null,
     mobile varchar(13) not null,
     email varchar(30) not null,
     addres varchar(20) not null,
     ADMIN char,
     CLIENTS char,
     PHOTOGRAPHERS char,
     constraint IDUSER primary key (user_id));


-- Constraints Section
-- ___________________ 

alter table ADMIN add constraint FKUSE_ADM_FK
     foreign key (user_id)
     references USER (user_id);

alter table APPOINTEMENT add constraint FKACCEPT
     foreign key (ACC_user_id)
     references PHOTOGRAPHERS (user_id);

alter table APPOINTEMENT add constraint FKREQUIRE
     foreign key (user_id)
     references CLIENTS (user_id);

alter table CLIENTS add constraint FKUSE_CLI_FK
     foreign key (user_id)
     references USER (user_id);

alter table IMAGES add constraint FKMAY BE
     foreign key (media_id)
     references MEDIA (media_id);

alter table IMAGES add constraint FKHAS
     foreign key (user_id)
     references PHOTOGRAPHERS (user_id);

alter table INVOICES_PH add constraint FKRECEIVE
     foreign key (user_id)
     references PHOTOGRAPHERS (user_id);

-- Not implemented
-- alter table MEDIA add constraint IDMEDIA_CHK
--     check(exists(select * from IMAGES
--                  where IMAGES.media_id = media_id)); 

alter table MEDIA add constraint FKACQUIRE
     foreign key (upload_id)
     references UPLOADS (upload_id);

alter table OFFERS add constraint FKOFF_SER
     foreign key (service_id)
     references SERVICES (service_id);

alter table OFFERS add constraint FKOFF_PHO
     foreign key (user_id)
     references PHOTOGRAPHERS (user_id);

alter table PHOTOGRAPHERS add constraint FKSITUATED
     foreign key (place_id)
     references PLACE (place_id);

alter table PHOTOGRAPHERS add constraint FKUSE_PHO_FK
     foreign key (user_id)
     references USER (user_id);

alter table RATINGS add constraint FKMAKE
     foreign key (user_id)
     references CLIENTS (user_id);

alter table RATINGS add constraint FKDESCRIBE
     foreign key (DES_user_id)
     references PHOTOGRAPHERS (user_id);

alter table SERVICE CHARGE add constraint FKPERFORMED
     foreign key (service_id)
     references SERVICES (service_id);

alter table SERVICE CHARGE add constraint FKBUYS
     foreign key (user_id)
     references CLIENTS (user_id);

-- Not implemented
-- alter table SERVICES add constraint IDSERVICE_CHK
--     check(exists(select * from OFFERS
--                  where OFFERS.service_id = service_id)); 

alter table SERVICES add constraint FKCAN BE
     foreign key (cat_id)
     references CATEGORIES (cat_id);

alter table UPLOADS add constraint FKHAS TYPE
     foreign key (cat_id)
     references CATEGORIES (cat_id);

alter table UPLOADS add constraint FKTHROUGH
     foreign key (user_id)
     references PHOTOGRAPHERS (user_id);

-- Not implemented
-- alter table UPLOADS add constraint IDUPLOADS_CHK
--     check(exists(select * from MEDIA
--                  where MEDIA.upload_id = upload_id)); 

alter table USER add constraint ISAUSER
     check((ADMIN is not null and CLIENTS is null and PHOTOGRAPHERS is null)
           or (ADMIN is null and CLIENTS is not null and PHOTOGRAPHERS is null)
           or (ADMIN is null and CLIENTS is null and PHOTOGRAPHERS is not null)); 


-- Index Section
-- _____________ 

