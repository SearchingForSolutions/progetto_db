# Progetto Basi di Dati 

### Breve descrizione:
Abbiamo deciso di realizzare un’applicazione web per sviluppare la nostra idea,tramite l'utilizzo dei seguenti linguaggi (PHP,HTML,CSS,JavaScript) come ambiente di test abbiamo usato XAMPP (testando sia su Windows che su Linux).

### Requisiti:
- ambiente LAMP,WAMP o XAMPP installato.
- permessi settati correttamente in ambiente Unix per le cartelle (htdocs o www) , in quanto si fanno upload sul server di immagini(.jpg|.png|.gif|.jpeg).
- PHP >= 5.6 e MySQL 

### Test:
- estrarre .zip contenente il progetto e metterlo nella root directory del webserver (htdocs o www)
- craere un nuovo database chiamato photo_advisor codifica caratteri utf8_bin.
- caricare nel db MySQL il file che si trova in app/db_sql_dump/photo_advisor.sql
- registrarsi nell'app o utilizzare una delle seguenti credenziali nel seguente formato (username,password)
- ADMIN(admin,admin).
- CLIENTE(client,client).
- FOTOGRAFO(ph,ph).
- enjoy :)

### Developers:

* Umberto Baldini(IT)
* Lorenzo Casini(IT)